import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the birthdayCakeCandles function below.
    static int birthdayCakeCandles(int[] ar) {
        int count=0,tallest=0;

        for (int i=0;i<ar.length;i++){
            if(tallest<ar[i]) {
                tallest = ar[i];
                count = 1;
            }
            else if(tallest==ar[i])
                count++;
        }

        return count;
    }



    public static void main(String[] args) throws IOException {

        int[] ar = {3,2,1,3};


        int result = birthdayCakeCandles(ar);

        System.out.println(result);
    }
}
